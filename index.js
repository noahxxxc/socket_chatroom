const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require('socket.io');
const io = new Server(server);

//聊天室內使用者名單
let chatUserList = []; 
let chatUserId = {} //id: userName

app.use(express.static('src'))
app.get('*', (req, res) => {
  res.sendfile(__dirname + "/src/index.html", () => {});
});

io.on('connection', (socket) => {
  let id = socket.id
  socket.on('chat-username', (name) => {
    if(!chatUserList.includes(name) && !(Object.values(chatUserId)).includes(name)) {
      chatUserList.push(name)
      chatUserId[id] = name //每個使用者用 id 區分，避免聊天印出訊息時，無法辨摁訊息是來自己方還是他人
      io.emit('user-check-result', {
        status: 'success',
        name: name,
        id: id
      })
    } else {
      io.emit('user-check-result', {
        status: 'fail',
        name: null,
        id: id
      })
    }
  });

  socket.on('chat-message', (data) => {
    data['name'] = chatUserId[data.id]
    io.emit('print-message', data)
  });

  socket.on("disconnect", (e) => {
    let nameIndex = chatUserList.indexOf(chatUserId[id])
    chatUserList.splice(nameIndex, 1)
    delete chatUserId[id]
  });
});

server.listen(5000, () => {
  console.log('listening on :5000');
});