let socket = io();

let nameInput = document.getElementById('username'),
    nameSubmit = document.getElementById('nameSubmit'),
    messageInput = document.getElementById('messageInput'),
    messageSubmit = document.getElementById('messageSubmit');
let myId = null;
let chatContent = document.getElementById('chatContent');

// function debounce(func, wait=250) {
//   let timer = null;
 
//   return () => {
//     let context = this;
//     let args = arguments;
 
//     clearTimeout(timer);
//     timer = setTimeout(() => {
//       func.apply(context, args);
//     }, wait)
//   }
// }
socket.on("connect", () => {
  myId = socket.id
});

nameSubmit.addEventListener('click', (e) => {
  e.preventDefault();
  if(nameInput.value) {
    socket.emit('chat-username', nameInput.value);
  }
});

socket.on('user-check-result', (data) => {
  if(data.status === 'success') {
    nameInput.value = '';
    document.getElementById('userNamePopup').classList.add('close');
    let memberJoin = document.createElement('p');
    memberJoin.classList.add('member-join');
    memberJoin.textContent = `${data.name} 進入聊天室`;
    chatContent.appendChild(memberJoin);
  } else {
    alert('名字已被使用，請更換其他名字');
  }
});

// 送出訊息(透過鍵盤 Enter)
messageInput.addEventListener('keydown', e => {
  if(e.keyCode === 13 && messageInput.value) {
    socket.emit('chat-message', {
      message: messageInput.value,
      id: myId
    });
    messageInput.value = '';
  }
});

// 送出訊息(點擊送出按鈕)
messageSubmit.addEventListener('click', () => {
  socket.emit('chat-message', {
    message: messageInput.value,
    id: myId
  });
  messageInput.value = '';
})


socket.on('print-message', (data) => {
  let chatBox = document.createElement('div');
  let chatDialog = document.createElement('div'),
      chatUserName = document.createElement('p'),
      chatMessage = document.createElement('div');

  chatBox.classList.add('chat-box');
  chatDialog.classList.add('chat-dialog');
  chatUserName.classList.add('chat-name');
  chatMessage.classList.add('chat-message');

  chatUserName.textContent = data.name;
  chatMessage.textContent = data.message;

  chatDialog.appendChild(chatMessage);
  if(data.id !== myId) {
    chatBox.appendChild(chatUserName);
  } else {
    chatBox.classList.add('me');
  }

  chatBox.appendChild(chatDialog);
  chatContent.appendChild(chatBox);

  window.scrollTo(0, document.body.scrollHeight);
});